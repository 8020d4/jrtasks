package com.javarush.task.task14.task1420;

import java.io.InputStreamReader;
import java.io.BufferedReader;

/*
НОД
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите два числа для нахождения НОД");

        try{
            int s1=Integer.parseInt(rd.readLine());
            int s2=Integer.parseInt(rd.readLine());
            System.out.println("НОД чисел "+s1+" и "+s2+" равен: "+NODfinder(s1,s2));
        }
        catch (Exception e) {
            System.out.println("Введите целые положительные числа!");
            System.out.println(e);
        }


    }

    public static int NODfinder(int a, int b){
        int c;
        if(a>b) {c=a;a=b;b=c;}
        int nod=1;
        for(int i=a;i>=1;i--){
            if(a%i==0&&b%i==0) {nod=i; break;}
        }
        return nod;
    }

}
