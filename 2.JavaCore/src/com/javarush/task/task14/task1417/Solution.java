package com.javarush.task.task14.task1417;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Валюты
*/

public class Solution {
    public static void main(String[] args) {
        Person ivan = new Person("Иван");
        ivan.setAllMoney(Arrays.asList(new Ruble(1230.22),new USD(29.34),new Hrivna(23.22)));

        for (Money money : ivan.getAllMoney()) {
            System.out.println(ivan.name + " имеет заначку в размере " + money.getAmount() + " " + money.getCurrencyName());
        }
    }

    static class Person {
        public String name;

        Person(String name) {
            this.name = name;
            this.allMoney = new ArrayList<>();
        }


        private List<Money> allMoney=new ArrayList<Money>();

        public void setAllMoney(List<Money> allMoney) {
            this.allMoney = allMoney;
        }

        public List<Money> getAllMoney() {
            return allMoney;
        }
    }
}
