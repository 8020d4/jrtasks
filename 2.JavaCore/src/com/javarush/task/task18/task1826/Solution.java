package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws Exception{
        FileInputStream fis=new FileInputStream(args[1]);
        FileOutputStream fos=new FileOutputStream(args[2]);
        ArrayList<Integer> bytes=new ArrayList<Integer>();
        int size=fis.available();
        while(fis.available()>0){
            bytes.add(fis.read());
        }
        fis.close();
        switch (args[0]){
            case "-e":
                for(int i=2;i<bytes.size();i=i+3){
                    int buffer=bytes.get(i);
                    bytes.set(i,bytes.get(0));
                    bytes.set(0,buffer);
                }
                for(int i:bytes){
                    fos.write(i);
                }
                fos.close();

                break;
            case "-d":

                for(int i=size-size%3-1;i>0;i=i-3){
                    int buffer=bytes.get(i);
                    bytes.set(i,bytes.get(0));
                    bytes.set(0,buffer);
                }
                for(int i:bytes){
                    fos.write(i);
                }
                fos.close();
                break;
        }
    }

}
