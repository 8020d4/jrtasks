package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file=rd.readLine();
        if(!args[0].equals("-c")) System.exit(1);
            BufferedReader fr = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            ArrayList<String> list = new ArrayList<String>();
            String line;
            while (!(line = fr.readLine()).equals(null)) {
                list.add(line);
            }
            ArrayList<Integer> ids = new ArrayList<Integer>();
            for (String s : list) {
                ids.add(Integer.parseInt(s.substring(0, 7)));
            }
            ArrayList<String> prodnames = new ArrayList<String>();
            for (String s : list) {
                prodnames.add(s.substring(8, 37));
            }
            ArrayList<Double> prices = new ArrayList<Double>();
            for (String s : list) {
                prices.add(Double.parseDouble(s.substring(38, 46)));
            }
            ArrayList<Integer> quantitys = new ArrayList<Integer>();
            for (String s : list) {
                quantitys.add(Integer.parseInt(s.substring(46, 50)));
            }
            Integer id = Collections.max(ids) + 1;
            String strid = null;
            if (id.toString().length() > 8) {
                id = Integer.parseInt(id.toString().substring(0, 7));
                strid = id.toString();
            } else {
                strid = id.toString();
                for (int i = 0; i < 8 - id.toString().length(); i++) {
                    strid = strid + " ";
                }
            }
            String prodname = null;
            if (args[1].length() > 30) {
                prodname = args[1].substring(0, 29);
            } else {
                prodname = args[1];
                for (int i = 0; i < 30 - args[1].length(); i++) {
                    prodname = prodname + " ";
                }
            }
            Double price;
            String strprice = null;
            if (args[2].length() > 8) {
                price = Double.parseDouble(args[2].substring(0, 7));
            } else {
                price = Double.parseDouble(args[2]);
                strprice = price.toString();
                for (int i = 0; i < 8 - args[2].length(); i++) {
                    strprice = strprice + " ";
                }
            }
            Integer quantity;
            String strquantity = null;
            if (args[3].length() > 4) {
                quantity = Integer.parseInt(args[3].substring(0, 3));
            } else {
                quantity = Integer.parseInt(args[3]);
                strquantity = quantity.toString();
                for (int i = 0; i < 4 - args[3].length(); i++) {
                    strquantity = strquantity + " ";
                }
            }
            list.add(strid + prodname + strprice + strquantity);
            for(String s:list){
                fw.write(s);
                fw.newLine();
            }
            rd.close();
            fw.close();
            fr.close();




    }
}
