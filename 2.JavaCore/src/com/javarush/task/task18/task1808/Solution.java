package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file=rd.readLine();
        String file1=rd.readLine();
        String file2=rd.readLine();
        FileInputStream fis=new FileInputStream(file);
        FileOutputStream fos1=new FileOutputStream(file1);
        FileOutputStream fos2=new FileOutputStream(file2);
        int s=fis.available();
        byte[] buffer;
        if(s%2!=0) buffer=new byte[(s+1)/2]; else buffer=new byte[s/2];
        fos1.write(buffer,0,fis.read(buffer));
        fos2.write(buffer,0,fis.read(buffer));
        rd.close();
        fis.close();
        fos1.close();
        fos2.close();

    }
}
