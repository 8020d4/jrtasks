package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));

        while(true){
        try {
            FileInputStream fis = new FileInputStream(rd.readLine());
            if(fis.available()<1000) {fis.close(); throw new DownloadException();}
            fis.close();
        }catch (IOException ex){System.out.println("Type correct file path!!!");}
    }
    }

    public static class DownloadException extends Exception {

    }
}
