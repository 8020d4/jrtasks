package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        FileOutputStream fos1=new FileOutputStream(rd.readLine());
        FileInputStream fis2=new FileInputStream(rd.readLine());
        FileInputStream fis3=new FileInputStream(rd.readLine());
        rd.close();
        while(fis2.available()>0){
                fos1.write(fis2.read());
            }
        while (fis3.available()>0){
                fos1.write(fis3.read());
            }
        fos1.close();
        fis2.close();
        fis3.close();



    }
}
