package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String path=rd.readLine();
        FileInputStream fis=new FileInputStream(path);

        int count=0;
        char ch=',';
        while(fis.available()>0){
            if((char)fis.read()==ch){
                count++;
            }

        }
        fis.close();
        System.out.println(count);
    }
}
