package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Самые редкие байты
*/

public class   Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String path=rd.readLine();
        Integer count;
        HashMap<Integer,Integer> map=new HashMap<Integer,Integer>();
        rd.close();
        ArrayList<Integer> ls=new ArrayList<Integer>();
        FileInputStream fis=new FileInputStream(path);
        Set<Integer> res=new HashSet<Integer>();
        while (fis.available()>0){
            ls.add(fis.read());
        }
        fis.close();
        for(Integer i=0;i<ls.size();i++){
            count=0;
            for(Integer l=0;l<ls.size();l++) {
                if(ls.get(i).equals(ls.get(l))) count++;
            }
            map.put(i,count);
        }
        for(Map.Entry entry:map.entrySet()){
            Integer k=(Integer)entry.getKey();
            Integer v=(Integer)entry.getValue();
            if(v==1) res.add(ls.get(k));
        }
        List result=new ArrayList(res);
        for(Integer i=0;i<result.size();i++){
            System.out.print(result.get(i)+" ");
        }


    }

}
