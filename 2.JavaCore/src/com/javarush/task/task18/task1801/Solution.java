package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis=new FileInputStream(rd.readLine());
        rd.close();
        int max=Integer.MIN_VALUE;

        while(fis.available()>0){
            max = Integer.max(fis.read(), max);
        }
        System.out.println(max);
        fis.close();
    }
}
