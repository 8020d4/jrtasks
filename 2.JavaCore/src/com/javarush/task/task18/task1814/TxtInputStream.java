package com.javarush.task.task18.task1814;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* 
UnsupportedFileName
*/

public class TxtInputStream extends FileInputStream {
    private static String fileName;

    TxtInputStream(String fileName) throws UnsupportedFileNameException,IOException{
        super(fileName);
        if((fileName.lastIndexOf(".txt")==-1)||(fileName.lastIndexOf(".txt")!=fileName.length()-4)) {super.close();throw new UnsupportedFileNameException();}
        else this.fileName=fileName;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException{
    }
}

