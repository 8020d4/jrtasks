package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file1=rd.readLine();
        String file2=rd.readLine();
        rd.close();
        FileInputStream fis1=new FileInputStream(file1);
        ArrayList<Byte> buffer=new ArrayList<Byte>();
        while (fis1.available()>0){
            buffer.add((byte)fis1.read());
        }
        fis1.close();
        FileOutputStream fos1=new FileOutputStream(file1);
        FileInputStream fis2=new FileInputStream(file2);
        while (fis2.available()>0){
            fos1.write(fis2.read());
        }
        for (int i=0;i<buffer.size();i++){
            fos1.write(buffer.get(i));
        }
        fis2.close();
        fos1.close();


    }
}
