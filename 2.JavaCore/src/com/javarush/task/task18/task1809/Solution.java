package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file1=rd.readLine();
        String file2=rd.readLine();
        FileInputStream fis=new FileInputStream(file1);
        FileOutputStream fos=new FileOutputStream(file2);
        ArrayList<Integer> ls=new ArrayList<Integer>();
        while(fis.available()>0){
            ls.add(fis.read());
        }
        for(int i=ls.size()-1;i>-1;i--){
            fos.write(ls.get(i));
        }
        rd.close();
        fis.close();
        fos.close();
    }
}
