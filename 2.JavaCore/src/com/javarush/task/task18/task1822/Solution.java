package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis=new FileInputStream(rd.readLine());
        BufferedReader fr=new BufferedReader(new InputStreamReader(fis));
        String line;
        while((line=fr.readLine())!=null){
            if(line.startsWith(args[0])){
                System.out.println(line);
            }
        }
        rd.close();
        fis.close();
        fr.close();
    }
}
