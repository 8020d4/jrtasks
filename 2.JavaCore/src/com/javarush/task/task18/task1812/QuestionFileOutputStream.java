package com.javarush.task.task18.task1812;

import java.io.*;

/* 
Расширяем AmigoOutputStream
*/

public class QuestionFileOutputStream implements AmigoOutputStream {
    private BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
    private AmigoOutputStream wrap;
    public QuestionFileOutputStream (AmigoOutputStream wrap){
        this.wrap=wrap;
    }

    @Override
    public void flush() throws IOException {
        wrap.flush();
    }

    @Override
    public void write(int b) throws IOException {
        wrap.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        wrap.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        wrap.write(b, off, len);
    }

    @Override
    public void close() throws IOException {
        System.out.println("Вы действительно хотите закрыть поток? Д/Н");
        String ans=rd.readLine();
        if(ans.equals("Д")) wrap.close();


    }
}

