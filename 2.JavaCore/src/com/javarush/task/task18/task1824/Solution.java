package com.javarush.task.task18.task1824;

/* 
Файлы и исключения
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader((System.in)));
        ArrayList<FileInputStream> fislist=new ArrayList<FileInputStream>();
        String line=null;
        while(true){
            try{
                line=rd.readLine();
                fislist.add(new FileInputStream(line));
            } catch (FileNotFoundException ex) {System.out.println(line); break;}
        }
        for(FileInputStream fis:fislist){
            fis.close();
        }
    }
}
