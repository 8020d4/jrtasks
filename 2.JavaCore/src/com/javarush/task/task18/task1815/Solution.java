package com.javarush.task.task18.task1815;

import java.util.List;

/* 
Таблица
*/

public class Solution {
    public class TableInterfaceWrapper implements ATableInterface{
        private ATableInterface atbl;
        public TableInterfaceWrapper(ATableInterface atbl){
            this.atbl=atbl;
        }
        @Override
        public void setModel(List rows) {
            System.out.println(rows.size());
            atbl.setModel(rows);


        }

        @Override
        public String getHeaderText() {
            return atbl.getHeaderText().toUpperCase();
        }

        @Override
        public void setHeaderText(String newHeaderText) {
            atbl.setHeaderText(newHeaderText);
        }
    }

    public interface ATableInterface {
        void setModel(List rows);

        String getHeaderText();

        void setHeaderText(String newHeaderText);
    }

    public static void main(String[] args) {
    }
}