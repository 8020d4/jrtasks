package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        int count=0;
        FileInputStream fis=new FileInputStream(args[0]);
        byte ch;
        int size=fis.available();
        while (fis.available()>0){
            ch=(byte)fis.read();
            if((ch>=65 )&&(ch<=90)||(ch>=97 )&&(ch<=122)) count++;

        }

        fis.close();
        System.out.println(count);

    }
}
