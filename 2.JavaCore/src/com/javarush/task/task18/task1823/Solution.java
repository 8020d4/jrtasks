package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.*;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException{
        ArrayList<ReadThread> threads=new ArrayList<ReadThread>();
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String line;
        while(true){
            line=rd.readLine();
            if(line.equals("exit")) break;
            ReadThread thread=new ReadThread(line);
            threads.add(thread);
        }
        rd.close();
        for(Thread s:threads){
            s.start();
        }
    }

    public static class ReadThread extends Thread {
        private String fileName;
        FileInputStream fis;
        Map<Integer, Integer> mod=new HashMap<Integer, Integer>();
        Map<Integer, Integer> res=new TreeMap<Integer, Integer>();
        ArrayList<Integer> e;

        public ReadThread(String fileName) {
            this.fileName=fileName;
            try{
                this.fis = new FileInputStream(fileName);
            } catch(Exception e){}
        }
        public void run(){
            try {
                while(this.fis.available()>0){
                    int b=(byte)this.fis.read();
                    if(mod.containsKey(b)){
                        int v=mod.get(b)+1;
                        mod.replace(b,v);
                    } else mod.put(b,1);
                }
                int maxK=0;
                int maxV=0;
                for(Map.Entry entry:mod.entrySet()){
                    int v=(int)entry.getValue();
                    int k=(int)entry.getKey();
                    if((int)entry.getValue()>maxV) {maxV=v;maxK=k;}
                }
                synchronized(resultMap){
                resultMap.put(this.fileName,maxK);
                }
                fis.close();
            }
            catch (FileNotFoundException ex){System.out.println(ex);}
            catch (IOException ex){System.out.println(ex);}

        }

    }
}
