package com.javarush.task.task18.task1803;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.Key;
import java.util.*;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String path=rd.readLine();
        FileInputStream fis=new FileInputStream(path);

        Integer count;
        Integer countMax=0;
        HashMap<Integer,Integer> map=new HashMap<Integer,Integer>();
        ArrayList<Integer> ls=new ArrayList<Integer>();
        Set<Integer> res=new HashSet<Integer>();
        while (fis.available()>0){
            ls.add(fis.read());
        }
        rd.close();
        fis.close();
        for(Integer i=0;i<ls.size();i++){
            count=0;
            for(Integer l=0;l<ls.size();l++) {
                if(ls.get(i).equals(ls.get(l))) count++;
            }
            countMax=Integer.max(count, countMax);
            map.put(i,count);
        }
        for(Map.Entry entry:map.entrySet()){
            Integer k=(Integer)entry.getKey();
            Integer buf=ls.get(k);
            Integer v=(Integer)entry.getValue();
            if(v.equals(countMax)) {
                res.add(buf);
            }
        }
        List result=new ArrayList(res);
        for(Integer i=0;i<result.size();i++){
            System.out.print(result.get(i)+" ");
        }


    }
}
