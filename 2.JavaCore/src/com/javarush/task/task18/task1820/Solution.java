package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file1=rd.readLine();
        String file2=rd.readLine();
        FileInputStream fis1=new FileInputStream(file1);
        FileOutputStream fos2=new FileOutputStream(file2);
        StringBuilder str=new StringBuilder();
        char ch;
        while(fis1.available()>0){
            ch=(char)fis1.read();
            if(ch!=' ') str.append(ch);
            if((ch==' ')||(fis1.available()==0)) {fos2.write(Integer.toString(Math.round(Float.parseFloat(str.toString()))).getBytes());fos2.write(' ');str.delete(0, str.length());}
        }
        fis1.close();
        fos2.close();

    }
}
