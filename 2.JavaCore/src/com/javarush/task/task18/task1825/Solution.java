package com.javarush.task.task18.task1825;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        Map<Integer, String> files=new TreeMap<Integer, String>();
        String line;
        String outName=null;
        while (!(line=rd.readLine()).equals("end")){
            if(!line.equals("end")) outName=line.substring(0,line.indexOf(".part"));
            files.put(Integer.parseInt(line.substring(line.indexOf(".part")+5)),line);
        }
        FileOutputStream fos=new FileOutputStream(outName);
        for(Map.Entry entry:files.entrySet()){
            FileInputStream fis=new FileInputStream((String)entry.getValue());
            byte[] buf=new byte[1000];
            while (fis.available()>0) {
                int mem = fis.read(buf);
                fos.write(buf,0,mem);
            }
            fis.close();
        }
        fos.close();
    }
}
