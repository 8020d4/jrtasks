package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String path=rd.readLine();
        FileInputStream fis=new FileInputStream(path);
        Set<Integer> ls=new HashSet<Integer>();
        while(fis.available()>0){
            ls.add(fis.read());
        }
        fis.close();
        Set<Integer> res=new TreeSet<Integer>();
        res.addAll(ls);
        List result=new ArrayList(res);
        for(Integer i=0;i<result.size();i++){
            System.out.print(result.get(i)+" ");
        }



    }
}
