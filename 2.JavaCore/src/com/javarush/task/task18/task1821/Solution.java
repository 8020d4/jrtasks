package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis=new FileInputStream(args[0]);
        Map<Character, Integer> res=new TreeMap<Character,Integer>();//result map
        ArrayList<Character> chars=new ArrayList<Character>();//read file
        while(fis.available()>0){
            chars.add((char)fis.read());
        }
        int count;
        for(int i=0;i<127;i++){
            count=0;
            for(int j=0;j<chars.size();j++){
                if((char)Integer.parseInt(String.valueOf(i))==chars.get(j)) count++;
            }
            if(count!=0)res.put((char)Integer.parseInt(String.valueOf(i)),count);
        }
        fis.close();
        for(Map.Entry entry: res.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
    }
}
