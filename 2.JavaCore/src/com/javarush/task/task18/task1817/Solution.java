package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException{
        FileInputStream fis=new FileInputStream(args[0]);
        int spases=0;
        int size=fis.available();
        while(fis.available()>0){
            if((char)fis.read()==' ') spases++;
        }
        System.out.printf("%.2f",(double) spases/size *100);
        fis.close();

    }
}
