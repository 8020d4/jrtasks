package com.javarush.task.task17.task1721;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) throws Exception{

            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
                String f1 = rd.readLine();
                String f2 = rd.readLine();
                allLines= Files.readAllLines(Paths.get(f1));
                forRemoveLines=Files.readAllLines(Paths.get(f2));
                System.in.close();

            } catch (IOException ex) {
                System.out.println("Type correct file path.");
            }
            try {
                    new Solution().joinData();
            }
            catch (CorruptedDataException e){
                System.out.println("CorruptedDataException");
            }

            System.out.close();
        }
    public void joinData() throws CorruptedDataException {

        boolean mute=false;
        while(!mute){
            if(allLines.containsAll(forRemoveLines)){
                allLines.removeAll(forRemoveLines);
                mute=true;
            }
            else {
                allLines.removeAll(allLines);
                throw new CorruptedDataException();
            }

        }

    }
}
