package com.javarush.task.task17.task1711;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {

        if(args.length>0){
            int l=args.length;
            int i=1;
            int index=l;

                switch (args[0]){
                case "-c":
                    synchronized (allPeople) {
                        l = args.length;
                        i = 1;
                        while (i <= l - 2) {
                            if (args[i + 1].equals("м")) {
                                try {
                                    SimpleDateFormat df = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                                    allPeople.add(Person.createMale(args[i], df.parse(args[i + 2])));
                                    System.out.println(allPeople.size()-1);
                                    i = i + 3;
                                } catch (Exception ex) {
                                    System.out.println(ex);
                                }
                            } else if (args[i + 1].equals("ж")) {
                                try {
                                    SimpleDateFormat df = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                                    allPeople.add(Person.createFemale(args[i], df.parse(args[i + 2])));
                                    System.out.println(allPeople.size()-1);
                                    i = i + 3;
                                } catch (Exception ex) {
                                    System.out.println(ex);
                                }
                            } else System.out.println("Type \"ж\" or \"м\"!!!");
                        }
                    }
                    break;
                case "-u":
                    synchronized (allPeople) {
                        l = args.length;
                        i = 1;
                        while (i <= l - 3) {
                            if (args[i +2].equals("м")) {
                                try {
                                    SimpleDateFormat df = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                                    allPeople.get(Integer.parseInt(args[i])).setBirthDay(df.parse(args[i + 3]));
                                    allPeople.get(Integer.parseInt(args[i])).setName(args[i + 1]);
                                    allPeople.get(Integer.parseInt(args[i])).setSex(Sex.MALE);
                                    i = i + 4;
                                } catch (Exception ex) {
                                    System.out.println(ex);
                                }
                            } else if (args[i + 2].equals("ж")) {
                                try {
                                    SimpleDateFormat df = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                                    allPeople.get(Integer.parseInt(args[i])).setBirthDay(df.parse(args[i + 3]));
                                    allPeople.get(Integer.parseInt(args[i])).setName(args[i + 1]);
                                    allPeople.get(Integer.parseInt(args[i])).setSex(Sex.FEMALE);
                                    i = i + 4;
                                } catch (Exception ex) {
                                    System.out.println(ex);
                                }
                            } else System.out.println("Type \"ж\" or \"м\"!!!");
                        }
                    }
                    break;
                case "-d":
                    synchronized (allPeople) {
                        l = args.length;
                        i = 1;
                        while (i <= l - 1) {
                            allPeople.get(Integer.parseInt(args[i])).deletePerson();
                            i++;
                        }
                    }
                    break;
                case "-i":
                    synchronized (allPeople) {
                        l = args.length;
                        i = 1;
                        while (i <= l - 1) {
                            allPeople.get(Integer.parseInt(args[i])).printInfo();
                            i++;
                        }
                    }
            }
        }
    }
}
