package com.javarush.task.task17.task1710;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public volatile static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        if(args.length>0){
            switch (args[0]){
                case "-c":
                    if(args[2].equals("м")){
                        try{
                        SimpleDateFormat oldDataFormat = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                        Date data = oldDataFormat.parse(args[3]);
                        allPeople.add(Person.createMale(args[1],data));
                        System.out.println(allPeople.size()-1);
                        }catch(Exception ex){System.out.println(ex);}
                    }
                    else if(args[2].equals("ж")){
                    try{
                        SimpleDateFormat oldDataFormat = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                        Date data = oldDataFormat.parse(args[3]);
                        allPeople.add(Person.createFemale(args[1],data));
                        System.out.println(allPeople.size()-1);
                    }catch(Exception ex){System.out.println(ex);}
                }
                else System.out.println("Wrong sex parameter!!! Try \"ж\" or \"м\"!!!");
                    break;
                case "-u":
                    if(args[3].equals("м"))
                    {
                        try{
                            SimpleDateFormat dd = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                            Date data = dd.parse(args[4]);
                            allPeople.remove(Integer.parseInt(args[1]));
                            allPeople.add(Person.createMale(args[2],data));

                        }catch(Exception ex){System.out.println(ex);}
                    } else
                    if(args[3].equals("ж"))
                    {
                        try{
                            SimpleDateFormat dd = new SimpleDateFormat("d/M/y", Locale.ENGLISH);
                            Date data = dd.parse(args[4]);
                            allPeople.remove(Integer.parseInt(args[1]));
                            allPeople.add(Person.createFemale(args[2],data));

                        }catch(Exception ex){System.out.println(ex);}
                    }
                    else System.out.println("Wrong sex parameter!!! Try \"ж\" or \"м\"!!!");
                    break;
                case "-d":
                    allPeople.get(Integer.parseInt(args[1])).setName(null);
                    allPeople.get(Integer.parseInt(args[1])).setSex(null);
                    allPeople.get(Integer.parseInt(args[1])).setBirthDay(null);
                    break;
                case "-i":
                    String sex="";
                    SimpleDateFormat dd = new SimpleDateFormat("d-MMM-y", Locale.ENGLISH);
                    Date data=allPeople.get(Integer.parseInt(args[1])).getBirthDay();
                    if(allPeople.get(Integer.parseInt(args[1])).getSex().equals(Sex.FEMALE)) sex="ж";else sex="м";
                    System.out.println(allPeople.get(Integer.parseInt(args[1])).getName()+" "+sex+" "+dd.format(data));
                    break;

            }
        }
    }
}
