package com.javarush.task.task19.task1927;

/* 
Контекстная реклама
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) throws Exception{
        PrintStream system =new PrintStream(System.out); //save
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        PrintStream context=new PrintStream(outputStream);
        System.setOut(context);
        testString.printSomething();
        BufferedReader rd=new BufferedReader(new InputStreamReader(new ByteArrayInputStream(outputStream.toByteArray())));
        System.setOut(system);
        int i=0;
        while(rd.ready()){
            if(i==2) {System.out.println("JavaRush - курсы Java онлайн");i=0;}
            else {
                {System.out.println(rd.readLine());i++;}
            }
        }

    }


    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }

}
