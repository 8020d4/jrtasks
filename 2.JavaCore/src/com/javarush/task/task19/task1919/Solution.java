package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        BufferedReader fr=new BufferedReader(new FileReader(args[0]));
        String line;
        Map<String, Double> res=new TreeMap<String, Double>();
        while((line=fr.readLine())!=null){
            String[] lines=line.split(" ");
            String k=lines[0];
            Double v=Double.parseDouble(lines[1]);
            if(res.containsKey(k)){
                v=v+res.get(k);
                res.remove(k);
                res.put(k,v);
            } else res.put(k,v);
        }
        fr.close();
        for(Map.Entry entry:res.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
    }
}
