package com.javarush.task.task19.task1922;

import javax.swing.*;

public class MainWindow extends JFrame {
    public MainWindow() throws Exception {
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        setVisible(true);
    }
}
