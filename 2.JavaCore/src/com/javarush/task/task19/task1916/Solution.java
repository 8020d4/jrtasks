package com.javarush.task.task19.task1916;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String f1=rd.readLine();
        String f2=rd.readLine();
        rd.close();
        BufferedReader fr1=new BufferedReader(new FileReader(f1));
        BufferedReader fr2=new BufferedReader(new FileReader(f2));
        while(fr1.ready()&fr2.ready()){
            fr1.mark(1000);
            fr2.mark(1000);
            String a=fr1.readLine();
            String b=fr2.readLine();
            if(b.equals(a)) lines.add(new LineItem(Type.SAME, a)); else {
                if(a.equals(fr2.readLine())) {
                    fr2.reset();
                    lines.add(new LineItem(Type.ADDED, fr2.readLine()));
                    
                }else {
                    lines.add(new LineItem(Type.REMOVED, a));
                    fr2.reset();
                }

            }
        }
        fr1.close();
        fr2.close();

        for(LineItem l:lines){
            System.out.println(l.type+" "+l.line);
        }
    }

    public static enum Type {
        ADDED, //добавлена новая строка
        REMOVED, //удалена строка
        SAME //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}