package com.javarush.task.task19.task1926;

/* 
Перевертыши
*/


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fr=new BufferedReader(new FileReader(rd.readLine()));
        rd.close();
        String line;
        while((line=fr.readLine())!=null){
            char[] a=line.toCharArray();
            StringBuilder st=new StringBuilder();
            for(int i=a.length-1;i>-1;i--){
                st.append(a[i]);
            }
            System.out.println(st.toString());
        }
        fr.close();
    }
}
