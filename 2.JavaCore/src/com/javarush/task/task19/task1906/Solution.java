package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file1=rd.readLine();
        String file2=rd.readLine();
        rd.close();
        FileReader fr=new FileReader(file1);
        FileWriter fw=new FileWriter(file2);
        while(fr.ready()){
            int data=fr.read();
            data=fr.read();
            fw.write(data);
        }
        fr.close();
        fw.close();
    }
}
