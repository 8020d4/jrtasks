package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException, FileNotFoundException{
        BufferedReader fr=new BufferedReader(new FileReader(args[0]));
        String line;
        Double max=0.0;
        Map<String, Double> res=new TreeMap<String, Double>();
        while((line=fr.readLine())!=null){
            String[] lines=line.split(" ");
            String k=lines[0];
            Double v=Double.parseDouble(lines[1]);
            if(res.containsKey(k)){
                v=v+res.get(k);
                res.remove(k);
                res.put(k,v);
            } else res.put(k,v);
            if(v>max) max=v;
        }
        fr.close();
        for(Map.Entry entry:res.entrySet()){
            if(entry.getValue().equals(max)){
                System.out.println(entry.getKey());
            }
        }
        }
    }
