package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file1=rd.readLine();
        String file2=rd.readLine();
        rd.close();
        BufferedReader fr=new BufferedReader(new FileReader(file1));
        BufferedWriter fw=new BufferedWriter(new FileWriter(file2, true));
        while(fr.ready()){
            String[] str=fr.readLine().split(" ");
            for(String a:str){
                try{
                fw.write(Integer.parseInt(a)+" ");}
                catch (Exception e){}
            }
        }
        fw.close();
        fr.close();
    }
}
