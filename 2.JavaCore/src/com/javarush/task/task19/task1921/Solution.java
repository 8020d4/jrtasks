package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws ParseException,IOException, FileNotFoundException{
        BufferedReader fr=new BufferedReader(new FileReader(args[0]));
        String line;
        SimpleDateFormat sdf=new SimpleDateFormat("d M yyyy",Locale.ENGLISH);
        while((line=fr.readLine())!=null){
            String[] lines=line.split(" ");
            String name=lines[0];
            for(int i=1;i<lines.length-3;i++){
                name=name+" "+lines[i];
            }
            PEOPLE.add(new Person(name, sdf.parse(lines[lines.length-3]+" "+lines[lines.length-2]+" "+lines[lines.length-1])));
        }
        fr.close();
    }
}
