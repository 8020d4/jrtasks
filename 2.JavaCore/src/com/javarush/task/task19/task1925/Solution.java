package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/*
Длинные слова
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader fr=new BufferedReader(new FileReader(args[0]));
        BufferedWriter fw=new BufferedWriter(new FileWriter(args[1]));
        String line;
        String[] words;
        String buffer=fr.readLine();
        while(true){
            line=fr.readLine();
            if(line!=null){
                words=buffer.split(" ");
                for(String s:words){
                    if(s.length()>6){
                        fw.write(s+",");
                    }
                }
                buffer=line;
            } else {
                words=buffer.split(" ");
                String res="";
                for(String s:words){
                    if(s.length()>6){
                        res=res+s+",";
                    }
                }
                fw.write(res.substring(0,res.length()-1));
            break;
            }
        }
        fw.close();
        fr.close();
    }
}

