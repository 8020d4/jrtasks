package com.javarush.task.task19.task1907;
/*
Считаем слово
*/

import jdk.nashorn.internal.runtime.regexp.joni.encoding.CharacterType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String file=rd.readLine();
        rd.close();
        int count=0;
        BufferedReader fr=new BufferedReader(new FileReader(file));
        Pattern world=Pattern.compile("\\bworld\\b");
        while(fr.ready()){
            Matcher line=world.matcher(fr.readLine());
            while(line.find()){
                count++;
            }
        }
        fr.close();

        System.out.println(count);


    }
}