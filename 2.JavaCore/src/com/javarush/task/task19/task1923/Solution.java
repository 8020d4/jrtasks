package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader fr=new BufferedReader(new FileReader(args[0]));
        BufferedWriter fw=new BufferedWriter(new FileWriter(args[1]));
        String line;
        String[] buf;
        Pattern p=Pattern.compile(".*[0-9]+.*");
        while((line=fr.readLine())!=null){
            buf=line.split(" ");
            for(int i=0;i<buf.length;i++){
                Matcher m=p.matcher(buf[i]);
                if(m.find()) fw.write(buf[i]+" ");
            }
        }
        fr.close();
        fw.close();
    }
}
