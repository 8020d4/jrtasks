package com.javarush.task.task19.task1904;


import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) throws Exception{
        /*PersonScannerAdapter psa=new PersonScannerAdapter(new Scanner(Paths.get("C:\\Users\\sambo\\Desktop\\AA_v3.log")));
        System.out.println(psa.read());
        System.out.println(psa.read());*/
    }

    public static class PersonScannerAdapter implements PersonScanner{
        private final Scanner fileScanner;
        PersonScannerAdapter(Scanner fileScanner){
            this.fileScanner=fileScanner;
        }

        @Override
        public Person read() throws IOException {
            String person=null;
            if(fileScanner.hasNext()){
                person=fileScanner.nextLine();
            }
            String[] data=person.split(" ",4);
            SimpleDateFormat df=new SimpleDateFormat("dd MM yyyy");
            Date dt=new Date();
            try {
                dt = df.parse(data[4]);
            }
            catch(ParseException ex){}
            return new Person(data[0],data[1],data[2],dt);
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}
