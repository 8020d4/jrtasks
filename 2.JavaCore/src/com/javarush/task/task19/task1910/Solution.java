package com.javarush.task.task19.task1910;

/* 
Пунктуация
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String f1=rd.readLine();
        String f2=rd.readLine();
        rd.close();
        BufferedReader fr=new BufferedReader(new FileReader(f1));
        BufferedWriter fw=new BufferedWriter(new FileWriter(f2));
        while(fr.ready()){
            String tmp = fr.readLine();
            fw.write(tmp.replaceAll("[\\pP\\s\\n]", ""));
        }
        fr.close();
        fw.close();
    }
}
