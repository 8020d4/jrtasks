package com.javarush.task.task20.task2003;


import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* 
Знакомство с properties
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception{
        //implement this method - реализуйте этот метод
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String line=rd.readLine();
        FileInputStream fis=new FileInputStream(line);
        load(fis);

    }

    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод
        Properties propertiesB=new Properties();
        /*for(Map.Entry entry:properties.entrySet()){
            properties.setProperty((String)entry.getKey(),(String)entry.getValue());
        }*/
        propertiesB.putAll(properties);
        propertiesB.store(outputStream,"");
    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод
        Properties propertiesA =new Properties();
        propertiesA.load(inputStream);
        propertiesA.forEach((key,value)->properties.put((String)key, (String)value));
    }

    public static void main(String[] args) throws Exception{
        Solution s=new Solution();
        s.fillInPropertiesMap();

    }
}
