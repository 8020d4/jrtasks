package com.javarush.task.task20.task2002;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile("C:\\Users\\sambo\\Desktop\\Code\\java\\JavaRushTasks\\JavaRushTasks\\2.JavaCore\\src\\com\\javarush\\task\\task20\\task2002\\gg.txt", null);
            OutputStream outputStream = new FileOutputStream("C:\\Users\\sambo\\Desktop\\Code\\java\\JavaRushTasks\\JavaRushTasks\\2.JavaCore\\src\\com\\javarush\\task\\task20\\task2002\\gg.txt");
            InputStream inputStream = new FileInputStream("C:\\Users\\sambo\\Desktop\\Code\\java\\JavaRushTasks\\JavaRushTasks\\2.JavaCore\\src\\com\\javarush\\task\\task20\\task2002\\gg.txt");

            /*
            private String firstName;
    private String lastName;
    private Date birthDate;
    private boolean isMale;
    private Country country;
             */
            SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
            JavaRush javaRush = new JavaRush();
            User users=new User();
            users.setFirstName("Ivan");
            users.setLastName("Petrov");
            users.setBirthDate(df.parse("29/11/2017 12:01:15.2"));
            users.setMale(true);
            users.setCountry(User.Country.OTHER);


            User user = new User();
            user.setFirstName("Rubi");
            user.setLastName("Rail");
            user.setBirthDate(new Date(1508944516168L));
            user.setMale(true);
            user.setCountry(User.Country.OTHER);

            User user1 = new User();
            user1.setFirstName("Vasa1");
            user1.setLastName("Peta1");
            user1.setBirthDate(new Date(1508944516163L));
            user1.setMale(true);
            user1.setCountry(User.Country.RUSSIA);

            User user3 = new User();
            user3.setFirstName("Solo");
            user3.setLastName("Han");
            user3.setBirthDate(new Date(1508944516169L));
            user3.setMale(true);
            user3.setCountry(User.Country.UKRAINE);
            javaRush.users.add(users);
            javaRush.users.add(user);
            javaRush.users.add(user1);
            javaRush.users.add(user3);
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            System.out.println(loadedObject.equals(javaRush));
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter saver=new PrintWriter(outputStream);
            saver.println("/SAVED_USER/");
            if(users.isEmpty()) saver.println("null"); else {
            for(User u:users){
                saver.println(u.getFirstName()+";"+u.getLastName()+";"+u.getBirthDate().getTime()+";"+u.isMale()+";"+u.getCountry());
            }}
            saver.println("end");
            saver.flush();
        }

        public void load(InputStream inputStream) throws Exception {
            List<User> buf=new ArrayList<>();
            BufferedReader loader=new BufferedReader(new InputStreamReader(inputStream));
            while(loader.ready()) {
                String data = loader.readLine();
                if (data.equals("/SAVED_USER/")) {
                    boolean f=true;
                    while(f) {
                        data=loader.readLine();
                        if(data.equals("end")) {f=false; break;}
                        if((data.equals("null"))) users=buf; else {
                        User u = new User();
                        u.setFirstName(data.split(";")[0]);
                        u.setLastName(data.split(";")[1]);
                        u.setBirthDate(new Date(Long.parseLong(data.split(";")[2])));
                        u.setMale(Boolean.parseBoolean(data.split(";")[3]));
                        if (data.split(";")[4].equals("UKRAINE"))
                            u.setCountry(User.Country.UKRAINE);
                        else if (data.split(";")[4].equals("RUSSIA"))
                            u.setCountry(User.Country.RUSSIA);
                        else
                            u.setCountry(User.Country.OTHER);
                        users.add(u);
                    }
                    }
                }
            }

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
