package com.javarush.task.task16.task1630;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static String firstFileName;
    public static String secondFileName;
    public static BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));

    static{
        try{
            firstFileName=rd.readLine();
            secondFileName=rd.readLine();
        }
        catch (Exception e){}
    }

    public static void main(String[] args) throws InterruptedException {
        systemOutPrintln(firstFileName);
        systemOutPrintln(secondFileName);
    }

    public static void systemOutPrintln(String fileName) throws InterruptedException {
        ReadFileInterface f = new ReadFileThread();
        f.setFileName(fileName);
        f.start();
        f.join();
        System.out.println(f.getFileContent());
    }

    public interface ReadFileInterface {

        void setFileName(String fullFileName);

        String getFileContent();

        void join() throws InterruptedException;

        void start();
    }

    public static class ReadFileThread extends Thread implements ReadFileInterface{
        private String readedString;
        private String result="";
        private String fileName;



        public void setFileName(String fullFileName){
            this.fileName=fullFileName;
        }
        public String getFileContent(){


            return result;
        }
        public void run(){
            try {
                BufferedReader fr=new BufferedReader(new FileReader(fileName));
                while((readedString=fr.readLine())!=null){
                    result=result+readedString+" ";
                }
                fr.close();
            }
            catch (Exception e){}

        }
    }
}
