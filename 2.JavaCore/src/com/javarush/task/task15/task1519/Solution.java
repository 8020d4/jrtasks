package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
        String gg="";
        ArrayList<String> list=new ArrayList<String>();
        while(!gg.equals(("exit"))){
            gg=rd.readLine();


            list.add(gg);
        }
        list.remove(list.size()-1);
        int i=0;
        while(i<list.size()){
            if(list.get(i).contains(".")) {try{print(Double.parseDouble((list.get(i))));} catch (Exception e){}} else {

                try{
                    int s=Integer.parseInt(list.get(i));
                    if(s>0&&s<128) print((short)s); else print(s);}
                    catch (Exception e){print(list.get(i));}

            }
            i++;
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
