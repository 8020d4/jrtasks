package com.javarush.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Статики-1
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();

     static{
        labels.put(22.1,"asd");
        labels.put(22.2,"asdf");
        labels.put(22.4,"dsg");
        labels.put(2.2,"ve");
        labels.put(22.3,"aawbysd");}


    public static void main(String[] args) {

        System.out.println(labels);
    }
}
